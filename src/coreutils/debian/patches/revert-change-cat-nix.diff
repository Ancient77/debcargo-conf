commit 14ac1e160fbf010c5f400396b451a3725526d58a
Author: Daniel Hofstetter <daniel.hofstetter@42dh.com>
Date:   Tue Mar 12 08:26:50 2024 +0100

    cat: adapt to type change of unistd::write()
    
    nix 0.28 changed "write(fd: RawFd, buf: &[u8]) -> Result<usize>" to "write<Fd: AsFd>(fd: Fd, buf: &[u8]) -> Result<usize>"

Index: coreutils/src/uu/cat/src/splice.rs
===================================================================
--- coreutils.orig/src/uu/cat/src/splice.rs
+++ coreutils/src/uu/cat/src/splice.rs
@@ -5,10 +5,7 @@
 use super::{CatResult, FdReadable, InputHandle};
 
 use nix::unistd;
-use std::os::{
-    fd::AsFd,
-    unix::io::{AsRawFd, RawFd},
-};
+use std::os::unix::io::{AsRawFd, RawFd};
 
 use uucore::pipes::{pipe, splice, splice_exact};
 
@@ -23,9 +20,9 @@ const BUF_SIZE: usize = 1024 * 16;
 /// The `bool` in the result value indicates if we need to fall back to normal
 /// copying or not. False means we don't have to.
 #[inline]
-pub(super) fn write_fast_using_splice<R: FdReadable, S: AsRawFd + AsFd>(
+pub(super) fn write_fast_using_splice<R: FdReadable>(
     handle: &InputHandle<R>,
-    write_fd: &S,
+    write_fd: &impl AsRawFd,
 ) -> CatResult<bool> {
     let (pipe_rd, pipe_wr) = pipe()?;
 
@@ -41,7 +38,7 @@ pub(super) fn write_fast_using_splice<R:
                     // we can recover by copying the data that we have from the
                     // intermediate pipe to stdout using normal read/write. Then
                     // we tell the caller to fall back.
-                    copy_exact(pipe_rd.as_raw_fd(), write_fd, n)?;
+                    copy_exact(pipe_rd.as_raw_fd(), write_fd.as_raw_fd(), n)?;
                     return Ok(true);
                 }
             }
@@ -55,7 +52,7 @@ pub(super) fn write_fast_using_splice<R:
 /// Move exactly `num_bytes` bytes from `read_fd` to `write_fd`.
 ///
 /// Panics if not enough bytes can be read.
-fn copy_exact(read_fd: RawFd, write_fd: &impl AsFd, num_bytes: usize) -> nix::Result<()> {
+fn copy_exact(read_fd: RawFd, write_fd: RawFd, num_bytes: usize) -> nix::Result<()> {
     let mut left = num_bytes;
     let mut buf = [0; BUF_SIZE];
     while left > 0 {
